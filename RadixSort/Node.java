// Class representing a node in a linked list
public class Node {
    public Entry entry;
    public Node next;

    public Node(Entry entry) {
        this.entry = entry;
        this.next = null;
    }
}