
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Create a new linked list
        LinkedList list = new LinkedList();

        // Prompt the user to choose a file
        System.out.println("Please choose a file from the following list:");
        System.out.println("1. areacode.txt");
        System.out.println("2. pokemon.txt");
        System.out.println("3. stooges.txt");
        System.out.println("4. years.txt");
        System.out.println("5. zipcodes.txt\n");
        System.out.print("Enter the number of the file you want to use: \n");

        // Read the user's choice
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        scanner.close();

        // Choose the file based on the user's input
        String file = "";
        switch (choice) {
            case 1:
                file = "./radixTestFiles/areacode.txt";
                break;
            case 2:
                file = "./radixTestFiles/pokemon.txt";
                break;
            case 3:
                file = "./radixTestFiles/stooges.txt";
                break;
            case 4:
                file = "./radixTestFiles/years.txt";
                break;
            case 5:
                file = "./radixTestFiles/zipcodes.txt";
                break;
            default:
                System.out.println("Invalid choice. Exiting program.\n");
                return;

        }

        // Read the data from the chosen file and populate the linked list with the
        // entries
        list.readFromFile(file);
        // Perform the Radix Sort on the linked list
        int numDigits = list.getNumDigits();
        list.radixSort(numDigits);

        // Print the sorted entries in the linked list
        list.print();

    }
}
