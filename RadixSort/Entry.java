// Class representing an entry in a linked list, with a key and a value
public class Entry {
    public String key;
    public String value;

    public Entry(String key, String value) {
        this.key = key;
        this.value = value;
    }
}