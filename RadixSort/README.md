# Radix Sort

This is a Radix Sort implementation in Java. It includes the following classes:

-   **LinkedList**: This class represents a linked list and includes methods for reading data from a file, adding and removing entries, and performing the Radix Sort.
-   **Entry**: This class represents a key-value pair in the linked list.
-   **Node**: This class represents a node in the linked list and includes a field for storing an entry.

## Using the Code

To use the code, create a new instance of the `LinkedList` class and call the `readFromFile(inputFile: String)` method, passing in the path to a file containing the data to be sorted. The data in the file should be in the form of key-value pairs, with the key and value separated by a comma. The file should start with a line containing the number of digits in the keys, and should end with the entry "END".

After reading the data from the file, call the `radixSort(numDigits: int)` method on the `LinkedList` object, passing in the number of digits in the keys as an argument. This will sort the linked list according to the keys.

To print the sorted list, call the `print()` method on the `LinkedList` object.

The `LinkedList` class has the following methods:

-   **readFromFile(inputFile: String)**: Reads the data from a file and populates the list with the entries.
-   **radixSort(numDigits: int)**: Performs the Radix Sort on the linked list.
-   **print()**: Prints the entries in the linked list.
-   **about()**: Returns a string describing the class.

The `Entry` class has the following fields:

-   **key**: The key of the entry.
-   **value**: The value of the entry.

The `Node` class has the following field:

-   **entry**: The entry stored in the node.