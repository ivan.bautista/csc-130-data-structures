import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LinkedList {
    // The head and tail of the linked list
    private Node head;
    private Node tail;

    // Returns a string describing the class
    public String about() {
        return "You can find my csc130 class notes at: https://csc130csus.ivanbautista.dev/ You can find the Repository of this project at: https://gitlab.com/ivan.bautista/csc-130-data-structures This is a RadixSort program written in Java, and was written by Isboset Ivan Bautista Librado, for the class of CSC 130.";
    }

    // The number of digits in the keys of the entries
    private int numDigits;

    // Returns the number of digits in the keys of the entries
    public int getNumDigits() {
        return numDigits;
    }

    // Reads the data from a file and populates the list with the entries
    public void readFromFile(String inputFile) {
        // Read the first line of the file to determine the number of digits in the keys
        numDigits = readNumDigitsFromFile(inputFile);

        // Read the remaining lines of the file
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            // Skip the first line (we already read it to determine the number of digits)
            reader.readLine();

            // Read the entries until we reach the "END" entry
            String line;
            while ((line = reader.readLine()) != null && !line.equals("END")) {
                // Split the line into the key and value
                String[] parts = line.split(",");
                String key = parts[0];
                String value = parts[1];

                // Create a new entry with the key and value, and add it to the list
                Entry entry = new Entry(key, value);
                addTail(entry);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Reads the first line of the file to determine the number of digits in the
    // keys
    private int readNumDigitsFromFile(String inputFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            // Read the first line of the file and parse it as an integer
            String line = reader.readLine();
            return Integer.parseInt(line);
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    // Returns the head node of the linked list
    public Node getHead() {
        return head;
    }

    // Adds an entry to the tail of the linked list
    public void addTail(Entry entry) {
        Node newNode = new Node(entry);
        if (isEmpty()) {
            head = newNode;
            tail = newNode;
        } else {
            tail.next = newNode;
            tail = newNode;
        }
    }

    // Removes the head node of the linked list
    public void removeHead() {
        if (isEmpty()) {
            return;
        }
        if (head == tail) {
            head = null;
            tail = null;
        } else {
            head = head.next;
        }
    }

    // Performs the Radix Sort on the linked list
    public void radixSort(int numDigits) {
        // Create an array of 10 linked lists to serve as the buckets
        LinkedList[] buckets = new LinkedList[10];
        for (int i = 0; i < 10; i++) {
            buckets[i] = new LinkedList();
        }

        // Perform the Radix Sort for each digit in the keys
        for (int d = 0; d < numDigits; d++) {
            // Move the entries from the current linked list to the appropriate bucket based
            // on the d-th digit of their keys
            while (!isEmpty()) {
                Node node = getHead();
                removeHead();

                int digit = getDigit(node.entry.key, d);
                buckets[digit].addTail(node.entry);
            }

            // Merge the entries from the buckets back into the linked list
            for (int i = 0; i < 10; i++) {
                while (!buckets[i].isEmpty()) {
                    Node node = buckets[i].getHead();
                    buckets[i].removeHead();
                    addTail(node.entry);
                }
            }
        }
    }

    // Returns the d-th digit of the given number, where d=0 corresponds to the
    // least significant digit
    private int getDigit(String number, int d) {
        int digit = Character.getNumericValue(number.charAt(number.length() - 1 - d));
        return digit;
    }

    // Returns true if the linked list is empty, false otherwise
    public boolean isEmpty() {
        return head == null;
    }

    // Prints the entries in the linked list
    public void print() {
        Node current = head;
        while (current != null) {
            System.out.println(current.entry.key + "," + current.entry.value);
            current = current.next;
        }
    }

}
