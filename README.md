# CSC 130 - Data Structures

This folder contains implementations of several data structures in Java:

-   **Linked List**: A linear data structure where each element is a separate object and is connected to the next element via a reference.
-   **Radix Sort**: An efficient sorting algorithm that sorts elements by the digits in their keys.
-   **Binary Search Tree**: A tree-based data structure where each node has at most two children and the key of each node is greater than the keys of its left children and less than the keys of its right children.


