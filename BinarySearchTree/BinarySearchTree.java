public class BinarySearchTree {
    // The root node of the tree
    private Node root;

    // Constructor for the BinarySearchTree class
    public BinarySearchTree() {
        root = null;
    }

    // Returns a string describing the class
    public String about() {
        return "You can find my csc130 class notes at: https://csc130csus.ivanbautista.dev/ You can find the Repository of this project at: https://gitlab.com/ivan.bautista/csc-130-data-structures This is a BinarySearchTree class written in Java, and was written by Isboset Ivan Bautista Librado, for the class of CSC 130.";
    }

    // Prints the tree using preorder tree traversal
    public void print() {
        // Only print the tree if the root node is not null
        if (root != null) {
            // Start the tree traversal at the root node with an indent of 0
            root.print(0);
        }
    }

    // Adds the given entry to the tree
    public void add(Entry entry) {
        // If the root node is null, set the root node to a new node with the given
        // entry
        if (root == null) {
            root = new Node(entry);
        } else {
            // Otherwise, add the entry to the tree using recursion starting at the root
            // node
            root.add(entry);
        }
    }

    // Finds a node with the given key and returns its value
    // If the node is not found, returns an empty string
    public String find(int key) {
        // If the root node is null, return an empty string
        if (root == null) {
            return "";
        } else {
            // Otherwise, find the node using recursion starting at the root node
            return root.find(key);
        }
    }
}
