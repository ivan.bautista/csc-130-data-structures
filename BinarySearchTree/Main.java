import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Create a new binary search tree
        BinarySearchTree tree = new BinarySearchTree();

        // Prompt the user to choose a file
        System.out.println("Please choose a file from the following list:");
        System.out.println("1. halloweenCalories.txt");
        System.out.println("2. halloweenCalories2.txt");
        System.out.println("3. pokemon.txt");
        System.out.println("4. years.txt");
        System.out.print("Enter the number of the file you want to use: \n");

        // Read the user's choice
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        scanner.close();

        // Choose the file based on the user's input
        String file = "";
        switch (choice) {
            case 1:
                file = "./treeTestFiles/halloweenCalories.txt";
                break;
            case 2:
                file = "./treeTestFiles/halloweenCalories2.txt";
                break;
            case 3:
                file = "./treeTestFiles/pokemon.txt";
                break;
            case 4:
                file = "./treeTestFiles/years.txt";
                break;
            default:
                System.out.println("Invalid choice. Exiting program.\n");
                return;
        }

        // Read the data from the chosen file and add the entries to the tree
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            // Read the entries until we reach the "END" entry
            String line;
            while ((line = reader.readLine()) != null && !line.equals("END")) {
                // Split the line into the key and value
                String[] parts = line.split(",");
                if (parts.length < 2) {
                    // Skip lines that do not contain a valid key-value pair
                    continue;
                }
                int key = Integer.parseInt(parts[0]);
                String value = parts[1];

                // Add the entry to the tree
                Entry entry = new Entry();
                entry.key = key;
                entry.value = value;
                tree.add(entry);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Print the tree
        tree.print();
    }
}
