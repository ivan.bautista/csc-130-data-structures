public class Node {
    // The value stored in this node
    public Entry value;

    // The left child of this node
    public Node left;

    // The right child of this node
    public Node right;

    // Constructor for creating a new Node with a given Entry
    public Node(Entry entry) {
        this.value = entry;
        this.left = null;
        this.right = null;
    }

    // Prints the tree rooted at this node with the given indentation level
    public void print(int indent) {
        for (int i = 0; i < indent; i++) {
            System.out.print(" ");
        }
        System.out.println("+--- " + value.key + ": " + value.value);
        if (left != null) {
            left.print(indent + 1);
        }
        if (right != null) {
            right.print(indent + 1);
        }
    }

    // Adds a new Entry to the tree rooted at this node
    public void add(Entry entry) {
        if (entry.key < this.value.key) {
            if (left != null) {
                left.add(entry);
            } else {
                left = new Node(entry);
            }
        } else if (entry.key > this.value.key) {
            if (right != null) {
                right.add(entry);
            } else {
                right = new Node(entry);
            }
        }
    }

    // Finds the value for a given key in the tree rooted at this node
    public String find(int key) {
        if (key == this.value.key) {
            return this.value.value;
        } else if (key < this.value.key) {
            if (left != null) {
                return left.find(key);
            } else {
                return "";
            }
        } else if (key > this.value.key) {
            if (right != null) {
                return right.find(key);
            } else {
                return "";
            }
        }
        return "";
    }
}
