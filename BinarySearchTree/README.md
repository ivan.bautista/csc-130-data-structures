# Binary Search Tree
This is a Binary Search Tree implementation in Java. It includes the following classes:

- **BinarySearchTree**: This class represents a binary search tree and includes methods for adding entries, finding entries, and printing the tree.
- **Entry**: This class represents a key-value pair in the binary search tree.
- **Node**: This class represents a node in the binary search tree and includes methods for adding entries and finding entries.
- **Main**: This class includes a main method that reads data from a file and adds it to the binary search tree.
## Using the Code
To use the code, run the **Main** class and follow the prompts to choose a file to read data from. The data in the file should be in the form of key-value pairs, with the key and value separated by a comma. The file should end with the entry "END".

The **BinarySearchTree** class has the following methods:

- **add(Entry entry)**: Adds the given entry to the tree.
- **find(int key)**: Finds a node with the given key and returns its value. If the node is not found, returns an empty string.
- **print()**: Prints the tree using preorder tree traversal.
- **about()**: Returns a string describing the class.
The Entry class has the following fields:

- **key**: The key of the entry.
- **value**: The value of the entry.
The **Node** class has the following methods:

- **add(Entry entry)**: Adds the given entry to the tree using recursion.
- **find(int key)**: Finds a node with the given key and returns its value using recursion. If the node is not found, returns an empty string.
- **print(int indent)**: Prints the tree using preorder tree traversal with the given indentation.