
public class LinkedList {
    // The head and tail of the linked list
    private Node head;
    private Node tail;

    // Returns a string describing the class
    public String about() {
        return "You can find my csc130 class notes at: https://csc130csus.ivanbautista.dev/ You can find the Repository of this project at: https://gitlab.com/ivan.bautista/csc-130-data-structures This is a LinkedList class written in Java, and was written by Isboset Ivan Bautista Librado, for the class of CSC 130.";
    }

    // Adds a new node to the head of the linked list
    public void addHead(String value) {
        // Create a new node with the given value
        Node newNode = new Node(value);
        // If the linked list is empty, set the head and tail to the new node
        if (isEmpty()) {
            head = newNode;
            tail = newNode;
        }
        // If the linked list is not empty, set the new node as the head and update the
        // next pointer of the new node
        else {
            newNode.next = head;
            head = newNode;
        }
    }

    // Adds a new node to the tail of the linked list
    public void addTail(String value) {
        // Create a new node with the given value
        Node newNode = new Node(value);
        // If the linked list is empty, set the head and tail to the new node
        if (isEmpty()) {
            head = newNode;
            tail = newNode;
        }
        // If the linked list is not empty, set the next pointer of the tail to the new
        // node and update the tail
        else {
            tail.next = newNode;
            tail = newNode;
        }
    }

    // Removes the head node of the linked list
    public String removeHead() {
        // If the linked list is empty, return an empty string
        if (isEmpty()) {
            return "";
        }
        // Store the value of the head node in a variable
        String result = head.value;
        // Update the head to the next node in the list
        head = head.next;
        // If the head is now null, set the tail to null as well
        if (head == null) {
            tail = null;
        }
        // Return the value of the removed head node
        return result;
    }

    // Removes the tail node of the linked list
    public String removeTail() {
        // If the linked list is empty, return an empty string
        if (isEmpty()) {
            return "";
        }
        // Store the value of the tail node in a variable
        String result = tail.value;
        // If the linked list has only one node, set the head and tail to null
        if (head == tail) {
            head = null;
            tail = null;
        }
        // If the linked list has more than one node, find the second to last node and
        // update the tail
        else {
            Node current = head;
            while (current.next != tail) {
                current = current.next;
            }
            current.next = null;
            tail = current;
        }
        // Return the value of the removed tail node
        return result;
    }

    // Prints the elements of the linked list
    public void print() {
        // If the list is empty, print an empty message
        if (isEmpty()) {
            System.out.println("The list is empty.");
        }
        // If the list is not empty, print the elements
        else {
            Node current = head;
            while (current != null) {
                System.out.print(current.value + " ");
                current = current.next;
            }
            System.out.println();
        }
    }

    // Returns true if the linked list is empty, false otherwise
    public boolean isEmpty() {
        return head == null;
    }

    // Checks if the linked list contains a specific value
    public boolean contains(String value) {
        // If the list is empty, return false
        if (isEmpty()) {
            return false;
        }
        // If the list is not empty, check if the head node contains the value
        else {
            return head.contains(value);
        }
    }
}
