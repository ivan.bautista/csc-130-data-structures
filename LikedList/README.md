# Linked List

This is a Linked List implementation in Java. It includes the following classes:

-   **LinkedList**: This class represents a linked list and includes methods for adding and removing entries, printing the list, and checking if the list is empty or contains a specific value.
-   **Node**: This class represents a node in the linked list and includes fields for storing a value and the next node in the list.

## Using the Code

To use the code, create a new instance of the `LinkedList` class and call the following methods as needed:

-   **addHead(value: String)**: Adds a new node with the given value to the head of the linked list.
-   **addTail(value: String)**: Adds a new node with the given value to the tail of the linked list.
-   **removeHead()**: Removes the head node of the linked list and returns its value.
-   **removeTail()**: Removes the tail node of the linked list and returns its value.
-   **print()**: Prints the elements of the linked list.
-   **isEmpty()**: Returns true if the linked list is empty, false otherwise.
-   **contains(value: String)**: Returns true if the linked list contains a node with the given value, false otherwise.
-   **about()**: Returns a string describing the class.

The `Node` class has the following fields:

-   **value**: The value stored in the node.
-   **next**: The next node in the linked list.
