public class Node {
    Node next;
    String value;

    public Node(String value) {
        this.value = value;
    }

    public boolean contains(String value) {
        // Check if the current node's value is the one we're looking for
        if (this.value.equals(value)) {
            return true;
        }
        // If not, check the next node in the list if it exists
        else if (next == null) {
            return false;
        } else {
            return next.contains(value);
        }
    }

    public void print() {
        System.out.print(value + " ");
        if (next != null) {
            next.print();
        }
    }
}
