import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Create a new linked list
        LinkedList list = new LinkedList();

        // Ask the user whether they want to use the predefined list, a list from a
        // file, or the "coffee.txt" list
        Scanner input = new Scanner(System.in);
        System.out.print(
                "Do you want to use the predefined list (enter P), a list from a file (enter F), or the coffee.txt list (enter C)? ");
        String choice = input.nextLine();

        // If the user chooses the predefined list, add the elements to the list and
        // print it
        if (choice.equalsIgnoreCase("P")) {
            list.addHead("Rabbit");
            list.addTail("Hornet");
            list.addTail("Squirrel");
            list.addTail("Turkey");
            list.print();
        }
        // If the user chooses a list from a file, read the file and add the elements to
        // the list
        else if (choice.equalsIgnoreCase("F")) {
            System.out.print("Enter the file path: ");
            String filePath = input.nextLine();

            try {
                // Read the file and add the elements to the list
                BufferedReader reader = new BufferedReader(new FileReader(filePath));
                String line;
                while ((line = reader.readLine()) != null) {
                    list.addTail(line);
                }
                reader.close();

                // Print the list
                list.print();
            } catch (IOException e) {
                System.out.println("Error reading file: " + e.getMessage());
            }
        }
        // If the user chooses the "coffee.txt" list, read the file and add the elements
        // to the list
        else if (choice.equalsIgnoreCase("C")) {
            try {
                // Read the "coffee.txt" file and add the elements to the list
                BufferedReader reader = new BufferedReader(new FileReader("coffee.txt"));
                String line;
                while ((line = reader.readLine()) != null) {
                    list.addTail(line);
                }
                reader.close();

                // Print the list
                list.print();
            } catch (IOException e) {
                System.out.println("Error reading file: " + e.getMessage());
            }
        }

        // If the user enters an invalid choice, print an error message
        else {
            System.out.println("Invalid choice. Please enter P, F, or C.");
        }

        input.close();

    }

}
